<?php
use App\Question;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\AnswersController;

/**
 * TODO : WRITE app.scss and then run npm run dev and update pagination on bootstrap-4blade.php(justify-content-center) and php artisan vendor publish to get that bootstrap ka file for pagination
 * install debugbar
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('welcome');
}
);
Auth::routes();
Route::resource('questions', 'QuestionsController')->except('show');
Route::get('questions/{slug}', 'QuestionsController@show')->name('questions.show');
Route::post('questions/{question}/favourite', 'FavouritesController@store')->name('questions.favourite');
Route::delete('questions/{question}/unfavourite', 'FavouritesController@destroy')->name('questions.unfavourite');
Route::post('questions/{question}/vote/{vote}', 'VotesController@voteQuestion')->name('questions.vote');
Route::post('answers/{answer}/vote/{vote}', 'VotesController@voteAnswer')->name('answers.vote');
Route::resource('questions.answers', 'AnswersController')->except(['index', 'show', 'create']);
Route::put('answers/{answer}/best-answer', 'AnswersController@bestAnswer')->name('answers.bestAnswer');
Route::get('/home', 'HomeController@index')->name('home');

