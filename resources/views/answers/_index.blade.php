<div class="row mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mt-8">
                            {{Str::plural('Answer', $question->answers_count)}}
                        </h3>
                    </div>
                    <div class="card-body">
                        @foreach($question->answers as $answer)
                            {!! $answer->body !!}
                            <div class="d-flex" style="justify-content:space-between">
                            <div class="d-flex mr-3">
                                <div class="d-flex flex-column">
                                @can('vote', $answer)
                                        <form action="{{route('answers.vote', [$answer->id, 1])}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn {{auth()->user()->hasAnswerUpVote($answer) ? 'text-dark' : 'text-black-50'}}">
                                            <i class="fa fa fa-caret-up fa-3x" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    @else
                                    <a href="{{route('login')}}" title="Up Vote" class="vote-up d-block text-center text-dark"><i class="fa fa-caret-up fa-3x" aria-hidden="true"></i></a>
                                    @endcan
                                    <h4 class="votes-count text-muted text-center m-0">{{$answer->votes_count}}</h4>
                                    @can('vote', $answer)
                                        <form action="{{route('answers.vote', [$answer->id, -1])}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn {{auth()->user()->hasAnswerDownVote($answer) ? 'text-dark' : 'text-black-50'}}">
                                            <i class="fa fa fa-caret-down fa-3x" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    @else
                                    <a href="{{route('login')}}" title="Up Vote" class="vote-up d-block text-center text-dark"><i class="fa fa-caret-up fa-3x" aria-hidden="true"></i></a>
                                    @endcan
                                </div>
                                <div class="mt-3 ml-4">
                                    @can('markAsBest', $answer)
                                        <form action="{{route('answers.bestAnswer', $answer->id)}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button type="submit" class="btn {{$answer->best_answer_status}}" title="Mark as best answer"><i class="fa fa-check fa-2x"></i></button>
                                        </form>
                                        @else
                                            @if($answer->is_best)
                                                <i class="fa fa-check fa-2x d-block text-success mb-2"></i>
                                            @endif
                                    @endcan
                                </div>
                                <div class="d-flex mt-4 ml-4">
                                <div>
                                    @can('update', $answer)
                                        <a href="{{route('questions.answers.edit', [$question->id, $answer->id])}}" class="btn btn-outline-primary">Edit</a>
                                    @endcan
                                    @can('delete', $answer)
                                    <form action="{{route('questions.answers.destroy', [$question->id, $answer->id])}}" method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="return confirm('are you sure?');" class="btn btn-outline-danger">Delete</button>
                                    </form>
                                    @endcan
                                </div>
                            </div>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="text-muted mb-2 text-right">
                                    Answered {{ $answer->created_date }}
                                </div>
                                <div class="d-flex mb-2">
                                    <div class="">
                                        <img src="{{$answer->author->avatar}}" alt="">
                                    </div>
                                    <div class="mt-2 ml-2">
                                        {{$answer->author->name}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>