<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'title'=> rtrim($faker->sentence(rand(5, 10)), '.'),
        'votes_count'=> rand(-10, 10),
        'views_count'=> rand(0, 10),
        // 'answers_count'=> rand(0, 10), Eloquent event handlling retrieved, creating, created, updating updated, deleting, deleted, restoring, restored to use eloquent event we need to register the even t to model 
        'body'=> $faker->paragraphs(rand(3, 7), true)
    ];
});
